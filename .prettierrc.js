module.exports = {
  semi: true,
  trailingComma: "all",
  singleQuote: false,
  jsxSingleQuote: false,
  tabWidth: 2,
  bracketSpacing: true,
  jsxBracketSameLine: false,
  arrowParens: "avoid",
  endOfLine: "lf",
};
