export const mobileSmall = 375;
export const mobile = 414;
export const tablet = 768;
export const desktopSmall = 1024;
export const desktop = 1440;
