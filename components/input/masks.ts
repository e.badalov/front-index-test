import { InputState } from "react-input-mask";

type TMaskModifier = (
  newState: InputState,
  oldState: InputState,
  userInput: string,
) => InputState;

interface IDictionary<T> {
  [key: string]: T;
}

const pad = (str: number) => (String(str).length === 1 ? `0${str}` : str);
const charMap: IDictionary<string> = {
  A: "А",
  B: "В",
  E: "Е",
  K: "К",
  M: "М",
  H: "Н",
  O: "О",
  P: "Р",
  C: "С",
  T: "Т",
  Y: "У",
  X: "Х",
};
const chars = new RegExp(`[${Object.keys(charMap).join("")}]`, "g");
const replacer = (char: string) => charMap[char] || char;

const formatChars = {
  D: "[0-9]",
  N: "[АВСЕНКМОРТХУABCEHKMOPTXYавсенкмортхуabcehkmoptxy0-9]",
  W: "[a-zA-Z0-9]",
  C: "[АВСЕНКМОРТХУABCEHKMOPTXYавсенкмортхуabcehkmoptxy]",
};

const plateNumberFormater: TMaskModifier = (newState, _oldState, userInput) => {
  if (!userInput) {
    return newState;
  }

  let { value } = newState;
  const { selection } = newState;

  value = value.toUpperCase().replace(chars, replacer);

  const parts = String(value).split(" ");
  const region = parts[3];

  if (region && region.length === 3) {
    parts[3] = pad(Number(region)) as string;
    value = parts.join(" ");
  }

  return {
    value,
    selection,
  };
};

export const masks = {
  PLATE_NUMBER_MASK: {
    mask: "C DDD CC",
    maskChar: null,
    formatChars,
    beforeMaskedValueChange: plateNumberFormater,
  },
  REGION_MASK: {
    mask: "DDD",
    maskChar: null,
    formatChars,
    beforeMaskedValueChange: plateNumberFormater,
  },
};
