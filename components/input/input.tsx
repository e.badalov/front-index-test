import React, { FunctionComponent, ChangeEvent } from "react";
import MaskedInput from "react-input-mask";
import classNames from "classnames";

import { masks } from "./masks";
import styles from "./index.module.scss";

interface IInputProps
  extends Omit<React.HTMLAttributes<HTMLInputElement>, "onChange"> {
  inputValue: string;
  onChange: (value: string, isValid?: boolean) => void;
  mask: "PLATE_NUMBER_MASK" | "REGION_MASK";
  alwaysShowMask?: boolean;
  validator?: (v: string) => boolean;
  disabled?: boolean;
  inputRef?: React.RefObject<HTMLInputElement>;
}

export const Input: FunctionComponent<IInputProps> = ({
  mask,
  inputValue,
  onChange,
  className,
  alwaysShowMask,
  placeholder,
  validator,
  id,
  disabled,
  inputRef,
}) => {
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const {
      target: { value },
    } = e;

    let isValid = validator ? validator(value) : true;

    onChange(value, isValid);
  };

  const defaultClassName = classNames(styles.input, className);
  const maskedProps = {
    alwaysShowMask,
    ...masks[mask],
  };

  const inputNode = (
    <input
      ref={inputRef}
      className={defaultClassName}
      placeholder={placeholder}
    />
  );

  return (
    <>
      <MaskedInput
        value={inputValue}
        onChange={handleChange}
        className={defaultClassName}
        {...maskedProps}
        placeholder={placeholder}
        data-autotest-id={id}
        disabled={disabled}
      >
        {() => inputNode}
      </MaskedInput>
    </>
  );
};

export * from "./validators";
