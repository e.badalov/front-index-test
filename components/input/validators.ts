const regex = (expression: RegExp) => (value: string): boolean =>
  expression.test(value as string);

const mobileNumberRegex = /\+7\s\d{3}\s\d{3}\s\d{2}\s\d{2}$/;

const plateNumberRegex = /[а-яA-Я]\s\d{3}\s[а-яA-Я]{2}$/;

export const validators = {
  MOBILE_NUMBER_MASK: regex(mobileNumberRegex),
  PLATE_NUMBER_VALIDATOR: regex(plateNumberRegex),
};
