import React from "react";

interface IProps {
  children: any;
  isLink: boolean;
  disabled: boolean;
  style: Record<string, unknown>;
  className: string;
  onClick?: () => void;
  ref?: React.RefObject<any>;
  href?: string;
  target?: string;
}

export const Component: React.FunctionComponent<IProps> = ({
  children,
  target,
  isLink,
  disabled,
  style,
  className,
  onClick,
  ref,
  href,
}): JSX.Element =>
  isLink ? (
    <a
      className={className}
      style={style}
      onClick={onClick}
      href={href}
      target={target}
    >
      {children}
    </a>
  ) : (
    <button
      type="button"
      className={className}
      style={style}
      onClick={onClick}
      ref={ref}
      disabled={disabled}
    >
      {children}
    </button>
  );
