import React from "react";
import classnames from "classnames";

import { Component } from "./component";

import styles from "./index.module.scss";

interface IProps {
  children: any;
  className?: string;
  disabled?: boolean;
  progress?: boolean;
  complete?: boolean;
  bordered?: boolean;
  progressWidth?: number;
  style?: Record<string, unknown>;
  onClick?: () => void;
  buttonRef?: React.RefObject<any>;
  href?: string;
}

export const Button: React.FunctionComponent<IProps> = ({
  className = "",
  children = "",
  disabled = false,
  progress = false,
  complete = false,
  bordered = false,
  progressWidth = 0,
  style = {},
  onClick,
  buttonRef,
  href,
}) => {
  const isLink = Boolean(href);
  const isExternal = href && !/^\/[^/]/.test(href);

  // btn_progress and btn_progress-complete btn_bordered btn-large btn-fixed

  const defaultClassname = classnames(
    styles.mLibBtn,
    className,
    bordered && styles.mLibBtn_bordered,
    progress && styles.mLibBtn_progress,
    complete && styles.mLibBtn_progressComplete,
    disabled && styles.mLibBtn_disabled,
  );
  return (
    <Component
      isLink={isLink}
      disabled={disabled}
      style={style}
      className={defaultClassname}
      onClick={onClick}
      ref={buttonRef}
      href={href}
      target={isExternal ? "_blank" : "_self"}
    >
      <span className={styles.mLibBtn__text}>{children}</span>
      <div
        className={styles.mLibBtn__line}
        style={{ width: `${progressWidth}%` }}
      />
      <div className={styles.mLibBtn__icon} />
    </Component>
  );
};
