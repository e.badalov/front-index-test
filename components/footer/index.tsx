import React, { FunctionComponent } from "react";

import TelegramIcon from "@/components/icons/telegram";
import WhatsAppIcon from "@/components/icons/whats-app";
import FacebookIcon from "@/components/icons/facebook";
import InstagramIcon from "@/components/icons/instagram";
import VkIcon from "@/components/icons/vk";
import YouTubeIcon from "@/components/icons/youtube";

import styles from "./index.module.scss";

export const Footer: FunctionComponent = () => (
  <footer className={styles.footer}>
    <div className={styles.content}>
      <div className={styles.line}>
        <div className={styles.main_content}>
          <div className={styles.navigation}>
            <a href="https://mafin.ru/kasko" className={styles.navigation_item}>
              КАСКО
            </a>
            <a href="https://mafin.ru/about" className={styles.navigation_item}>
              О нас
            </a>
            <a href="https://mafin.ru/osago" className={styles.navigation_item}>
              ОСАГО
            </a>
            <a
              href="https://mafin.ru/about-partner"
              className={styles.navigation_item}
            >
              О партнере
            </a>
            <a
              href="https://mafin.ru/insurance-policy"
              className={styles.navigation_item}
            >
              Документы и правила страхования
            </a>
            <a
              href="https://mafin.ru/about#office-map"
              className={styles.navigation_item}
            >
              Офисы продаж и обслуживания
            </a>
          </div>
          <div className={styles.column_wrapper}>
            <div className={styles.column}>
              <a className={styles.marked_item} href="tel:88005551555">
                8 800 555 1 555
              </a>
              <span className={styles.description_item}>
                Служба заботы о клиентах 24/7
              </span>
              <div className={styles.social_newtwork_row}>
                <TelegramIcon className={styles.social_newtwork_icon} />
                <WhatsAppIcon className={styles.social_newtwork_icon} />
              </div>
            </div>
            <div className={styles.column}>
              <span className={styles.marked_item}>
                ул.&nbsp;Ленинская&nbsp;Cлобода,
                д.&nbsp;19&nbsp;стр.&nbsp;6&nbsp;этаж&nbsp;4
              </span>
              <span className={styles.description_item}>Юридический адрес</span>
              <div className={styles.social_newtwork_row}>
                <VkIcon className={styles.social_newtwork_icon} />
                <FacebookIcon className={styles.social_newtwork_icon} />
                <InstagramIcon className={styles.social_newtwork_icon} />
                <YouTubeIcon className={styles.social_newtwork_icon} />
              </div>
            </div>
          </div>
        </div>
        <div className={styles.app_stores}>
          <a
            href="https://redirect.appmetrica.yandex.com/serve/963748596463720024"
            className={styles.app_store_logo_wrppaer}
          >
            <img
              src="/next-public-fast/app-store.svg"
              alt="App Store"
              className={styles.app_store}
              width="1px"
              height="1px"
            />
          </a>
          <a
            href="https://redirect.appmetrica.yandex.com/serve/387288124440507854"
            className={styles.app_store_logo_wrppaer}
          >
            <img
              src="/next-public-fast/google-play.svg"
              alt="Google Play"
              className={styles.google_play}
              width="1px"
              height="1px"
            />
          </a>
          <a
            href="https://appgallery8.huawei.com/#/app/C102763887"
            className={styles.app_store_logo_wrppaer}
          >
            <img
              src="/next-public-fast/huawei-store.svg"
              alt="Huawei"
              className={styles.huawei_store}
              width="1px"
              height="1px"
            />
          </a>
        </div>
      </div>
      <div className={styles.line}>
        <p className={styles.license}>
          Страховые услуги оказывает ООО «Абсолют Страхование».
          <br /> Лицензия на осуществление страховой деятельности
          СИ&nbsp;№ 2496,&nbsp;ОС№ 2496-03.
          <br />
          2018-2021 ООО&nbsp;«АбсолютТех».
        </p>
        <div className={styles.payments_systems}>
          <img
            className={styles.visa}
            src="/next-public-fast/visa.svg"
            alt="Visa"
            width="1px"
            height="1px"
          />
          <img
            className={styles.mastercard}
            src="/next-public-fast/mastercard.svg"
            alt="Master Card"
            width="1px"
            height="1px"
          />
          <img
            className={styles.mir}
            src="/next-public-fast/mir.svg"
            alt="МИР"
            width="1px"
            height="1px"
          />
          <img
            className={styles.apple_pay}
            src="/next-public-fast/apple-pay.svg"
            alt="Apple pay"
            width="1px"
            height="1px"
          />
          <img
            className={styles.google_pay}
            src="/next-public-fast/google-pay.svg"
            alt="Google pay"
            width="1px"
            height="1px"
          />
        </div>
      </div>
    </div>
  </footer>
);
