import React, { FunctionComponent } from "react";
import classnames from "classnames";

import Vk from "@/assets/icons/social-networks/vk.svg";

import styles from "./index.module.scss";

interface IProps {
  className?: string;
}

const VkIcon: FunctionComponent<IProps> = ({ className }) => (
  <a href="https://vk.com/mafinrussia">
    <Vk className={classnames(styles.icon, className)} />
  </a>
);

export default VkIcon;
