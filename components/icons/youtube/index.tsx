import React, { FunctionComponent } from "react";
import classnames from "classnames";

import YouTube from "@/assets/icons/social-networks/youtube.svg";

import styles from "./index.module.scss";

interface IProps {
  className?: string;
}

const YouTubeIcon: FunctionComponent<IProps> = ({ className }) => (
  <a href="https://www.youtube.com/MafinRussia">
    <YouTube className={classnames(styles.icon, className)} />
  </a>
);

export default YouTubeIcon;
