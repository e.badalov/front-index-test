import React, { FunctionComponent } from "react";
import classnames from "classnames";

import WhatsApp from "@/assets/icons/social-networks/whats-app.svg";

import styles from "./index.module.scss";

interface IProps {
  className?: string;
}

const WhatsAppIcon: FunctionComponent<IProps> = ({ className }) => (
  <a href="https://wa.me/79998151244">
    <WhatsApp className={classnames(styles.icon, className)} />
  </a>
);

export default WhatsAppIcon;
