import React, { FunctionComponent } from "react";
import classnames from "classnames";

import Instagram from "@/assets/icons/social-networks/instagram.svg";

import styles from "./index.module.scss";

interface IProps {
  className?: string;
}

const InstagramIcon: FunctionComponent<IProps> = ({ className }) => (
  <a href="https://www.instagram.com/mafinrussia">
    <Instagram className={classnames(styles.icon, className)} />
  </a>
);

export default InstagramIcon;
