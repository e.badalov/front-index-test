import React, { FunctionComponent } from "react";
import classnames from "classnames";

import Telegram from "@/assets/icons/social-networks/telegram.svg";

import styles from "./index.module.scss";

interface IProps {
  className?: string;
}

const TelegramIcon: FunctionComponent<IProps> = ({ className }) => (
  <a href="https://t.me/Mafin_sales_bot">
    <Telegram className={classnames(styles.icon, className)} />
  </a>
);

export default TelegramIcon;
