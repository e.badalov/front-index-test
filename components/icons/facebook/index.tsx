import React, { FunctionComponent } from "react";
import classnames from "classnames";

import Facebook from "@/assets/icons/social-networks/facebook.svg";

import styles from "./index.module.scss";

interface IProps {
  className?: string;
}

const FacebookIcon: FunctionComponent<IProps> = ({ className }) => (
  <a href="https://www.facebook.com/mafinrussia">
    <Facebook className={classnames(styles.icon, className)} />
  </a>
);

export default FacebookIcon;
