import React, { FunctionComponent } from "react";

import { Footer } from "@/components/footer";
import { Header } from "@/components/header";

import styles from "./index.module.scss";

export const Layout: FunctionComponent = ({ children }) => (
  <>
    <Header />
    <main className={styles.main}>{children}</main>
    <Footer />
  </>
);
