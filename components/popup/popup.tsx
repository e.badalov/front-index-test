import React, { FunctionComponent } from "react";

import Close from "@/assets/icons/close.svg";

import styles from "./popup.module.scss";

export interface IPopupProps {
  autotestId: string;
  show?: boolean;
  logoLink?: string;
  onClose?: () => void;
}

export const Popup: FunctionComponent<IPopupProps> = ({
  children,
  autotestId,
  show,
  onClose,
  logoLink,
}) => {
  React.useEffect(() => {
    document.body.style.overflow = show ? "hidden" : "auto";
  }, [show]);

  if (!show) return null;

  const stopPropagation = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    e.stopPropagation();
  };

  return (
    <div className={styles.popup} data-autotest-id={autotestId}>
      <div aria-hidden className={styles.popup__fade} onClick={onClose}>
        <div
          aria-hidden
          className={styles.popup__content}
          onClick={stopPropagation}
        >
          <a href={logoLink}>
            <div className={styles.popup__logo} />
          </a>
          <Close className={styles.popup__close} onClick={onClose} />
          {children}
        </div>
      </div>
    </div>
  );
};
