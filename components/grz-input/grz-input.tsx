import React, { FunctionComponent, useState } from "react";
import classNames from "classnames";

import { Input, validators } from "../input/input";
import styles from "./index.module.scss";

export interface IGrzInputProps {
  setGrzData: (value: string) => void;
  grz: string;
  setRegionData: (value: string) => void;
  region: string;
}

export const GRZInput: FunctionComponent<IGrzInputProps> = ({
  setGrzData,
  setRegionData,
  grz,
  region,
}) => {
  const [isGRZValid, setIsGRZValid] = useState(false);
  const regionInputRef: React.RefObject<HTMLInputElement> = React.createRef();

  const onGrzChange = (value: string, isValid?: boolean) => {
    setIsGRZValid(true);
    setGrzData(value);
    if (isValid && regionInputRef && regionInputRef.current) {
      regionInputRef.current.focus();
    }
  };

  const onRegionChange = (value: string) => {
    setRegionData(value);
  };

  return (
    <div className={styles.grz__enter}>
      <Input
        inputValue={grz}
        onChange={onGrzChange}
        className={classNames(styles.grz__input, styles.common__input)}
        mask="PLATE_NUMBER_MASK"
        placeholder="А 000 АА"
        validator={validators.PLATE_NUMBER_VALIDATOR}
        id="grz-number"
      />
      <Input
        inputRef={regionInputRef}
        inputValue={region}
        onChange={onRegionChange}
        className={classNames(styles.region__input, styles.common__input)}
        mask="REGION_MASK"
        placeholder="000"
        id="grz-region"
        disabled={!isGRZValid}
      />
    </div>
  );
};
