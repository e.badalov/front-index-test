import React, { FunctionComponent, useState } from "react";
import classnames from "classnames";

import QuestionIcon from "@/assets/icons/question.svg";

import styles from "./tooltip.module.scss";

export interface ITooltipProps {
  tabletColor?: boolean;
  grey?: boolean;
  right?: boolean;
}

export const Tooltip: FunctionComponent<ITooltipProps> = ({
  children,
  tabletColor,
  grey,
  right,
}) => {
  let width;
  if (typeof window !== "undefined") {
    width = window.innerWidth;
  }

  const [windowWidth, setWindowWidth] = useState(width);
  const [show, setShow] = useState(false);

  const toggleOverflow = () => {
    if (document.body.style.overflow !== "hidden") {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "visible";
    }
  };

  const toggleShow = () => {
    if (typeof window !== "undefined") {
      setWindowWidth(window.innerWidth);
    }
    if (typeof windowWidth !== "undefined" && windowWidth < 1024) {
      setShow(!show);
      toggleOverflow();
    }
  };

  return (
    <>
      <div className={styles.tooltip}>
        <div
          className={classnames(styles.icon, {
            [styles.icon_color]: tabletColor,
            [styles.icon_grey]: grey,
          })}
          onClick={toggleShow}
          onKeyDown={toggleShow}
          role="button"
          tabIndex={0}
        >
          <QuestionIcon />
        </div>
        <div
          className={classnames(styles.content_desktop, {
            [styles.content_right]: right,
          })}
        >
          {children}
        </div>
      </div>

      <div
        className={classnames(
          styles.popup_fade,
          { [styles.block]: show },
          { [styles.none]: !show },
        )}
      >
        <div className={styles.content}>
          {children}
          <div
            role="button"
            tabIndex={0}
            className={styles.tooltip__close}
            onKeyDown={() => {
              setShow(false);
              toggleOverflow();
            }}
            onClick={() => {
              setShow(false);
              toggleOverflow();
            }}
          >
            Закрыть
          </div>
        </div>
      </div>
    </>
  );
};
