import React, { FunctionComponent } from "react";

import styles from "./step.module.scss";

export interface IStepProps {
  count: number;
  descriptionLineOne: string;
  descriptionLineTwo: string;
  descriptionLineThree: string;
}

export const Step: FunctionComponent<IStepProps> = ({
  count,
  descriptionLineOne,
  descriptionLineTwo,
  descriptionLineThree,
  children,
}) => (
  <div className={styles.step}>
    <div className={styles.step_mobile_left}>
      <div className={styles.step_count}>{count}</div>
      <div className={styles.step_description}>
        {descriptionLineOne}
        <br />
        {descriptionLineTwo}
        <br />
        {descriptionLineThree}
      </div>
    </div>
    <div className={styles.step_popupLink}>{children}</div>
  </div>
);
