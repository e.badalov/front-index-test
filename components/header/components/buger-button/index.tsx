import React, { FunctionComponent } from "react";
import classnames from "classnames";

import styles from "./index.module.scss";

interface IProps {
  opened: boolean;
  onClick: () => void;
}

export const BurgerButton: FunctionComponent<IProps> = ({
  opened,
  onClick,
}) => {
  const linesClassName = classnames(styles.lines, {
    [styles["lines--open"]]: opened,
  });

  return (
    <div
      className={styles.wrapper}
      onClick={onClick}
      onKeyPress={onClick}
      role="button"
      tabIndex={0}
    >
      <div className={styles.button}>
        <div className={linesClassName} />
      </div>
    </div>
  );
};
