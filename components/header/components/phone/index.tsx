import React, { FunctionComponent } from "react";

import styles from "./index.module.scss";

export const Phone: FunctionComponent = () => (
  <a href="tel:88005551555" className={styles.phone}>
    8 (800) 555-1-555
  </a>
);
