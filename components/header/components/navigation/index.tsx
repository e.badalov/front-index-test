import React, { FunctionComponent, useState } from "react";
import classnames from "classnames";
import { useRouter } from "next/router";

import { MenuItem } from "./components/menu-item";

import styles from "./index.module.scss";

interface IProps {
  showed: boolean;
}

const kaskoSubItems = [
  {
    href: "/kasko/calc",
    text: "Рассчитать КАСКО",
  },
  {
    href: "/kasko",
    text: "Преимущества КАСКО",
  },
  {
    href: "/kasko/protect",
    text: "От чего защищает КАСКО",
  },
  {
    href: "/kasko/stoa",
    text: "Адреса сервисных центров",
  },
  {
    href: "/kasko/rules",
    text: "Правила страхования",
  },
];
const osagoSubItems = [
  {
    href: "/osago/calc",
    text: "Рассчитать ОСАГО",
  },
  {
    href: "/osago",
    text: "Преимущества ОСАГО",
  },
  {
    href: "/osago/europrotocol",
    text: "Как заполнить Европротокол",
  },
  {
    href: "/osago/insurance-case",
    text: "Заявить о страховом случае",
  },
];
const aboutSubItems = [
  {
    href: "/about",
    text: "О нас",
  },
  {
    href: "/about#office-map",
    text: "Офисы продаж и обслуживания",
  },
];

export const Navigation: FunctionComponent<IProps> = ({ showed }) => {
  const [openedItemId, setOpenedItemId] = useState("");
  const { pathname: currentUrl } = useRouter();

  const navigationClassName = classnames(styles.navigation, {
    [styles["navigation--isOpen"]]: showed,
  });

  const mainPageLinkClassName = classnames(styles.main_link, {
    [styles["main_link--active"]]: currentUrl === "/",
  });

  const onItemClick = (id: string) => {
    const isCurrent = id === openedItemId;
    const isSmthOpen = !!openedItemId;

    if (isCurrent) {
      setOpenedItemId("");
    } else if (isSmthOpen) {
      setOpenedItemId("");
      setTimeout(() => setOpenedItemId(id), 200);
    } else {
      setOpenedItemId(id);
    }
  };

  return (
    <nav className={navigationClassName}>
      <a href="/" className={mainPageLinkClassName}>
        Главная
      </a>

      <MenuItem
        name="КАСКО"
        subItems={kaskoSubItems}
        isOpen={openedItemId === "KASKO"}
        onClick={onItemClick}
        id="KASKO"
      />
      <MenuItem
        name="ОСАГО"
        subItems={osagoSubItems}
        isOpen={openedItemId === "OSAGO"}
        onClick={onItemClick}
        id="OSAGO"
      />
      <MenuItem
        name="О нас"
        subItems={aboutSubItems}
        isOpen={openedItemId === "ABOUT"}
        onClick={onItemClick}
        id="ABOUT"
      />
    </nav>
  );
};
