import React, { FunctionComponent } from "react";
import classnames from "classnames";
import { useRouter } from "next/router";

import ArrowIcon from "@/assets/icons/arrow.svg";

import styles from "./index.module.scss";

interface ISubItem {
  href: string;
  text: string;
  target?: string;
}

interface IProps {
  name: string;
  subItems: Array<ISubItem>;
  onClick: (v: string) => void;
  id: string;
  isOpen: boolean;
}

export const MenuItem: FunctionComponent<IProps> = ({
  name,
  subItems,
  isOpen,
  onClick,
  id,
}) => {
  const { pathname: currentUrl } = useRouter();
  const isActive = subItems.some(({ href }) => href === currentUrl);

  const menuSubItems = subItems.map(({ href, text, target }) => (
    <a
      href={href}
      target={target}
      className={classnames(styles.subItem, {
        [styles["subItem--active"]]: href === currentUrl,
      })}
      key={href}
    >
      {text}
    </a>
  ));

  const subMenuClassName = classnames(styles.sub, {
    [styles["sub--isOpen"]]: isOpen,
  });

  const arrowClassName = classnames(styles.arrow, {
    [styles["arrow--isOpen"]]: isOpen,
    [styles["arrow--isActive"]]: isActive,
  });

  const lineClassName = classnames(styles.line, {
    [styles["line--active"]]: isActive,
  });

  const onItemClick = () => {
    onClick(id);
  };

  return (
    <div className={styles.item}>
      <div
        className={lineClassName}
        onClick={onItemClick}
        onKeyPress={onItemClick}
        role="menuitem"
        tabIndex={0}
      >
        {name}
        <div className={styles.arrow__wrapper}>
          <ArrowIcon className={arrowClassName} />
        </div>
      </div>
      <nav className={subMenuClassName}>
        <div className={styles.sub__wrapper}>{menuSubItems}</div>
      </nav>
    </div>
  );
};
