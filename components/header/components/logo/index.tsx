/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { FunctionComponent } from "react";

import styles from "./index.module.scss";

export const Logo: FunctionComponent = () => (
  // eslint-disable-next-line jsx-a11y/anchor-has-content
  <a href="/" className={styles.logo} />
);
