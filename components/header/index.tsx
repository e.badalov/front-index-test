import React, { FunctionComponent, useState } from "react";

import { BurgerButton } from "./components/buger-button";
import { Phone } from "./components/phone";
import { Logo } from "./components/logo";
import { Navigation } from "./components/navigation";

import styles from "./index.module.scss";

export const Header: FunctionComponent = () => {
  const [isMenuOpen, setIsOpen] = useState(false);

  const toggleMenu = () => setIsOpen(!isMenuOpen);

  return (
    <>
      <header className={styles.header__wrapper}>
        <div className={styles.header}>
          <div className={styles.header__content}>
            <Logo />
            <Navigation showed={isMenuOpen} />
            <div className={styles.header__icons}>
              <Phone />
              <BurgerButton opened={isMenuOpen} onClick={toggleMenu} />
            </div>
          </div>
        </div>
      </header>
      <div className={styles.header__dummy} />
    </>
  );
};
