import React, { FunctionComponent, useState } from "react";
import Router from "next/router";
import classnames from "classnames";

import { Button } from "@/components/button";
import { Tooltip } from "@/components/tooltip";
import { GRZInput } from "@/components/grz-input";

import styles from "./grz-form.module.scss";

interface IProps {
  title: string;
  greyTitle?: boolean; // if need tablet title color to be gray
  btntext: string;
  bgcolor: string;
  className?: string;
}

export const GRZform: FunctionComponent<IProps> = ({
  title,
  greyTitle,
  btntext,
  bgcolor,
  className,
}) => {
  const [grz, setGrz] = useState("");
  const [region, setRegion] = useState("");

  const onButtonClick = () => {
    let plateRegex = /[АВЕКМНОРСТУХ] \d{3} [АВЕКМНОРСТУХ]{2} ([0-9]{2,3})/;
    function setScenario(text: string) {
      let scenario = JSON.stringify([
        {
          single: true,
          condition: {
            when: {
              type: { equal: "QUESTION" },
              camunda_task_id: {
                some: ["Task_GetPlate", "Task_Osago_GetPlate"],
              },
            },
            then: {
              send: {
                object: {
                  type: "TEXT",
                  data: null,
                  text,
                  media: null,
                },
              },
            },
          },
        },
      ]);
      window.localStorage.setItem("DIALOG_SCENARIO", scenario);
    }
    const plateNumber = `${grz} ${region}`;
    if (plateRegex.test(plateNumber)) {
      setScenario(plateNumber);
    }
    Router.push("/kasko/calc");
  };

  const updateGrz = (value: string) => {
    setGrz(value);
  };

  const updateRegion = (value: string) => {
    setRegion(value);
  };

  return (
    <div
      className={classnames(styles.grz, styles.grz__top, className)}
      style={{ backgroundColor: bgcolor }}
    >
      <p
        data-autotest-id="grz-plate-title"
        className={classnames(styles.grz__title, {
          [styles.grz__title_color]: greyTitle,
        })}
      >
        {title}
      </p>
      <div className={styles.grz__actions}>
        <GRZInput
          setGrzData={updateGrz}
          grz={grz}
          setRegionData={updateRegion}
          region={region}
        />
        <div className={styles.grz__row}>
          <div data-autotest-id="grz-kasko-tooltip" className={styles.tooltip}>
            <Tooltip tabletColor={!!greyTitle}>
              <div>
                <ul className={styles.tooltip__list}>
                  <li
                    className={classnames(
                      styles.tooltip__list_item,
                      styles.tooltip__list_item_car,
                    )}
                  >
                    Если у вас новый авто, просто нажмите на кнопку «Рассчитать
                    КАСКО»
                  </li>
                  <li
                    className={classnames(
                      styles.tooltip__list_item,
                      styles.tooltip__list_item_lock,
                    )}
                  >
                    Ваши данные под надежной защитой и обрабатываются в строгом
                    соответствии с законодательством РФ
                  </li>
                </ul>
              </div>
            </Tooltip>
          </div>
          <div data-autotest-id="kasko-calc-btn">
            <Button onClick={onButtonClick} className={styles.grz__btn}>
              {btntext}
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};
