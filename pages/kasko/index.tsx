import React from "react";
import Head from "next/head";

import { Protect } from "@/containers/kasko/protect";
import { Promo } from "@/containers/kasko/promo";
import { Buy } from "@/containers/kasko/buy";
import { SpecialOffers } from "@/containers/kasko/special-offers";
import { Cars } from "@/containers/kasko/cars";
import { Stoa } from "@/containers/kasko/stoa";
import { Advantages } from "@/containers/kasko/advantages";
import { PaymentType } from "@/containers/kasko/payment-type";
import { Video } from "@/containers/kasko/video";
import { App } from "@/containers/kasko/app";

import styles from "./kasko.module.scss";

const Kasko: React.FunctionComponent = () => (
  <>
    <Head>
      <title>
        КАСКО онлайн-калькулятор 2021 - рассчитать стоимость за 2 минуты — Mafin
      </title>
      <meta
        name="description"
        content="Оцените преимущества КАСКО от Mafin. Персональная цена ниже до 30%. Круглосуточная поддержка. Бесплатные опции: эвакуатор, аварийный комиссар и направление на ремонт к официальному дилеру уже через час."
      />
      <meta
        name="keywords"
        content="каско, калькулятор, онлайн, полис каско, расчет каско, автострахование, страхование авто, мафин, mafin"
      />
      <meta name="robots" content="index, follow" />
      <meta property="og:type" content="website" />
      <meta property="og:site_name" content="Mafin" />
      <meta property="og:url" content="https://mafin.ru/kasko" />
      <meta
        property="og:title"
        content="КАСКО по персональной цене — ниже до 30%"
      />
      <meta
        property="og:description"
        content="Оцените преимущетсва КАСКО от Mafin: поддержка 24/7, эвакуатор, аварийный комиссар, направление на ремонт уже через час."
      />
      <meta name="twitter:card" content="summary" />
      <meta
        name="twitter:title"
        content="Купить КАСКО по персональной цене — Mafin"
      />
      <meta
        property="og:image"
        content="https://mafin.ru/themes/mafin-site/assets/images/share/kasko-calc.jpg"
      />
      <meta
        property="og:image:secure_url"
        content="https://mafin.ru/themes/mafin-site/assets/images/share/kasko-calc.jpg"
      />
      <meta
        name="twitter:image"
        content="https://mafin.ru/themes/mafin-site/assets/images/share/tw.png"
      />
    </Head>
    <Promo />
    <div className={styles.container}>
      <Protect />
      <Advantages />
      <Buy />
      <PaymentType />
      <SpecialOffers />
      <Cars />
      <Stoa />
      <Video />
      <App />
    </div>
  </>
);

export default Kasko;
