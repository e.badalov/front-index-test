import React, { FunctionComponent } from "react";

import { Popup } from "@/components/popup";
import { Step } from "@/components/step";

import styles from "./steps.module.scss";

type PopupIndex = 1 | 2 | 3 | -1;

export const Steps: FunctionComponent = () => {
  const [shownPopup, setShownPopup] = React.useState<PopupIndex>(-1);

  const showPopup = (index: PopupIndex) => () => setShownPopup(index);
  const hidePopup = () => setShownPopup(-1);

  return (
    <section className={styles.settlement}>
      <div className={styles.content}>
        <div className={styles.container}>
          <div className={styles.steps}>
            <Step
              count={1}
              descriptionLineOne="Рассчитайте"
              descriptionLineTwo="цену и&nbsp;настройте"
              descriptionLineThree="свой полис"
            >
              <div
                aria-hidden
                onClick={showPopup(1)}
                className={styles.link__title}
              >
                Подробнее
              </div>
              <Popup
                show={shownPopup === 1}
                onClose={hidePopup}
                autotestId="steps_popup_1"
              >
                <div className={styles.popup__content}>
                  <div className={styles.popup__content__header}>
                    <div className={styles.step_count}>1</div>
                    <p className={styles.popup__content__title}>
                      Рассчитайте цену
                      <br />
                      и&nbsp;настройте свой полис
                    </p>
                  </div>
                  <p className={styles.popup__content__text}>
                    Цена рассчитывается из множества факторов — это позволяет
                    сделать ее максимально выгодной для вас.
                  </p>
                  <p className={styles.popup__content__text}>
                    Укажите госномер авто, ФИО, дату рождения
                    и&nbsp;водительский стаж, а&nbsp;затем выберите страховое
                    покрытие, размер франшизы и&nbsp;понравившиеся опции.
                  </p>
                </div>
              </Popup>
            </Step>

            <Step
              count={2}
              descriptionLineOne="Загрузите документы"
              descriptionLineTwo="и&nbsp;проведите"
              descriptionLineThree="онлайн-осмотр авто"
            >
              <div
                aria-hidden
                onClick={showPopup(2)}
                className={styles.link__title}
              >
                Подробнее
              </div>
              <Popup
                show={shownPopup === 2}
                onClose={hidePopup}
                autotestId="steps_popup_2"
              >
                <div className={styles.popup__content}>
                  <div className={styles.popup__content__header}>
                    <div className={styles.step_count}>2</div>
                    <p className={styles.popup__content__title}>
                      Загрузите документы
                      <br />
                      и&nbsp;проведите онлайн-осмотр авто
                    </p>
                  </div>
                  <p className={styles.popup__content__text}>
                    Сделайте фото и отправьте нам:
                  </p>
                  <div className={styles.popup__content__text}>
                    <ul className={styles.popup__content__list}>
                      <li className={styles.popup__content__list_item}>
                        СТС - свидетельство о регистрации транспортного
                        средства;
                      </li>
                      <li className={styles.popup__content__list_item}>
                        ПТС - паспорт транспортного средства (в некоторых
                        случаях);
                      </li>
                      <li className={styles.popup__content__list_item}>
                        Водительские удостоверения (всех водителей);
                      </li>
                      <li className={styles.popup__content__list_item}>
                        Паспорт страхователя и собственника авто.
                      </li>
                    </ul>
                  </div>
                  <div className={styles.popup__content__text}>
                    Для проведения онлайн-осмотра скачайте приложение Mafin.
                    Снимите фото и&nbsp;видео по&nbsp;нашим инструкциям. Если
                    возникнут сложности направим к&nbsp;вам на&nbsp;помощь
                    специалиста. Если у&nbsp;вас уже есть действующий полис
                    КАСКО&nbsp;&mdash; онлайн-осмотр может
                    не&nbsp;потребоваться.
                  </div>
                </div>
              </Popup>
            </Step>

            <Step
              count={3}
              descriptionLineOne="Оплатите полис"
              descriptionLineTwo="онлайн и&nbsp;получите"
              descriptionLineThree="его на&nbsp;email"
            >
              <div
                aria-hidden
                onClick={showPopup(3)}
                className={styles.link__title}
              >
                Подробнее
              </div>
              <Popup
                show={shownPopup === 3}
                onClose={hidePopup}
                autotestId="steps_popup_3"
              >
                <div className={styles.popup__content}>
                  <div className={styles.popup__content__header}>
                    <div className={styles.step_count}>3</div>
                    <p className={styles.popup__content__title}>
                      Оплатите полис онлайн
                      <br />
                      и&nbsp;получите его на&nbsp;email
                    </p>
                  </div>
                  <p className={styles.popup__content__text}>
                    Оплатите полис банковской картой&nbsp;&mdash; это быстро
                    и&nbsp;безопасно. Мы&nbsp;вышлем его на&nbsp;электронную
                    почту и&nbsp;сохраним в&nbsp;вашем личном кабинете
                    приложения Mafin
                  </p>
                </div>
              </Popup>
            </Step>
          </div>
        </div>
      </div>
    </section>
  );
};
