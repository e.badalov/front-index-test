import React from "react";

import { Card } from "./components";
import styles from "./payment-type.module.scss";

export const PaymentType: React.FunctionComponent = () => (
  <div className={styles.wrapper} data-autotest-id="payment-type">
    <h2 className={styles.title} data-autotest-id="h2-payment-type__title">
      Удобный способ оплаты
    </h2>
    <div className={styles.hint} data-autotest-id="payment-type__hint">
      С Mafin вы можете оплатить КАСКО сразу или по частям. Без переплат
      и скрытых комиссий
      <br />
    </div>
    <div
      className={styles.show__more}
      data-autotest-id="payment-type__show-more"
    >
      <a href="https://mafin.ru/kasko/rassrochka">Подробнее о рассрочке</a>
    </div>
    <div className={styles.cards} data-autotest-id="payment-type__cards">
      <Card
        img="/next-public-fast/payment-type/monthly-payment.svg"
        mobileImg="/next-public-fast/payment-type/m-monthly-payment.svg"
        title="Ежемесячный платеж"
        subtitle={`Раз в месяц равными \nчастями`}
      />
      <Card
        img="/next-public-fast/payment-type/two-payments.svg"
        mobileImg="/next-public-fast/payment-type/m-two-payments.svg"
        title="2 платежа"
        subtitle={`50% в день оформления, \n50% — через три месяца`}
      />
      <Card
        img="/next-public-fast/payment-type/one-payment.svg"
        mobileImg="/next-public-fast/payment-type/m-one-payment.svg"
        title="1 платеж"
        subtitle={`Разовая оплата в день \nоформления`}
      />
    </div>
  </div>
);
