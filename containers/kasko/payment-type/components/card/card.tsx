import React from "react";
import styles from "./card.module.scss";

export interface CardProps {
  img: string;
  mobileImg: string;
  title: string;
  subtitle: string;
}

export const Card: React.FunctionComponent<CardProps> = ({
  img,
  mobileImg,
  title,
  subtitle,
}) => (
  <div className={styles.wrapper} data-autotest-id="card">
    <div className={styles.img__wrapper}>
      <img
        src={img}
        className={styles.img}
        alt=""
        data-autotest-id="card__img"
        width="1px"
        height="1px"
      />
      <img
        src={mobileImg}
        className={styles.mobile__img}
        alt=""
        data-autotest-id="card__mobile-image"
        width="1px"
        height="1px"
      />
    </div>
    <div className={styles.label} data-autotest-id="card__label">
      <div className={styles.title} data-autotest-id="card__title">
        {title}
      </div>
      <div className={styles.subtitle} data-autotest-id="card__subtitle">
        {subtitle}
      </div>
    </div>
  </div>
);
