import React, { FunctionComponent } from "react";

import { Button } from "@/components/button";

import { Cards } from "./components";
import styles from "./protect.module.scss";

export const Protect: FunctionComponent = () => (
  <section className={styles.protect}>
    <div className={styles.protect__flex}>
      <div className={styles.protect__info}>
        <h3
          className={styles.protect__title}
          data-autotest-id="h3-kasko-protect__title"
        >
          КАСКО с&nbsp;Mafin&nbsp;&mdash; ваша защита
          <br />
          от&nbsp;непредвиденных расходов
        </h3>
        <p className={styles.protect__description}>
          Обеспечим ремонт после ДТП, повреждений злоумышленниками, пожара,
          стихийных бедствий, падения предметов и&nbsp;других случаев.
          <br />
          Возместим ущерб при угоне и&nbsp;полной гибели вашего автомобиля.
        </p>
      </div>
      <Cards />
    </div>
    <div className={styles.protect__link}>
      <Button
        href="/kasko/protect"
        data-autotest-id="link_to__kasko_protect"
        bordered
      >
        От чего защищает КАСКО
      </Button>
    </div>
  </section>
);
