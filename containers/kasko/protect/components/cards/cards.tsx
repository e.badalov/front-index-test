import React, { FunctionComponent } from "react";

import styles from "./cards.module.scss";
import { Card } from "./components/card";

export const Cards: FunctionComponent = () => {
  const cardsData = [
    {
      href: "/kasko/protect/accident",
      autotestId: "accident",
      title: "ДТП",
      image: "dtp.svg",
    },
    {
      href: "/kasko/protect/car-theft",
      autotestId: "car-theft",
      title: "Угон",
      image: "ufo.svg",
    },
    {
      href: "/kasko/protect/vandalism",
      autotestId: "vandalism",
      title: "Вандализм",
      image: "vandalism.svg",
    },
    {
      href: "/kasko/protect/disaster",
      autotestId: "disaster",
      title: "Стихийное бедствие",
      image: "carsink.svg",
    },
    {
      href: "/kasko/protect/falling",
      autotestId: "falling",
      title: "Падение предметов",
      image: "falling.svg",
    },
    {
      href: "/kasko/protect/animals",
      autotestId: "animals",
      title: "Действия животных",
      image: "animals.svg",
    },
  ];

  return (
    <div className={styles.cards}>
      {cardsData.map(card => (
        <Card
          key={card.autotestId}
          href={card.href}
          title={card.title}
          image={card.image}
          autotestId={card.autotestId}
        />
      ))}
    </div>
  );
};
