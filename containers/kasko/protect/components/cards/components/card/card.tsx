import React, { FunctionComponent } from "react";

import styles from "./card.module.scss";

export interface ICardProps {
  href: string;
  autotestId: string;
  title: string;
  image: string;
}

export const Card: FunctionComponent<ICardProps> = ({
  href,
  title,
  image,
  autotestId,
}) => (
  <a
    href={href}
    data-autotest-id={`link__kasko-protect-${autotestId}`}
    className={styles.card__container}
  >
    <div className={styles.card}>
      <p className={styles.card__title}>{title}</p>
      <div
        className={styles.card__image}
        style={{
          backgroundImage: `url("/next-public-fast/images/kasko/kasko-protect/${image}")`,
        }}
      />
    </div>
  </a>
);
