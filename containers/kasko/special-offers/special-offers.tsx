import React, { FunctionComponent } from "react";

import styles from "./special-offers.module.scss";
import { Slider } from "./components/slider";

export const SpecialOffers: FunctionComponent = () => (
  <div className={styles.specialOffers}>
    <div className={styles.specialOffers__container}>
      <h2
        data-autotest-id="h2-special-offers__title"
        className={styles.specialOffers__title}
      >
        Акции и спецпредложения от Mafin
      </h2>
      <Slider data-autotest-id="special-offers__slider" />
    </div>
  </div>
);
