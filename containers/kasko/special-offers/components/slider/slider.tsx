import React, { FunctionComponent } from "react";
import SwiperCore, { Pagination, Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import classNames from "classnames";

import { Offer } from "./components/offer";

import styles from "./slider.module.scss";

SwiperCore.use([Navigation, Pagination]);

const offers = [
  {
    label: "Спецпредложение",
    title: "Страхование водителя и пассажиров",
    description: "Выплатим до 500 000 руб. при наступлении несчастного случая",
    additional:
      "Страхование водителя и пассажиров можно включить при оформлении КАСКО",
    href: "/kasko/health",
    desktopImg: "d-kasko-offer-additional-insurance.jpg",
    tabletImg: "m-kasko-offer-additional-insurance.jpg",
  },
  {
    label: "Спецпредложение",
    title: "Дополнительная автогражданская ответственность (ДАГО)",
    description: "Расширим автогражданскую ответственность до 1 000 000 руб.",
    additional: "ДАГО можно включить при оформлении КАСКО",
    href: "/kasko/dago",
    desktopImg: "d-kasko-offer-dago.jpg",
    tabletImg: "m-kasko-offer-dago.jpg",
  },
  {
    label: "Спецпредложение",
    title: "Сэкономьте до 80% c мини-КАСКО",
    description: "Оптимальная защита для уверенных водителей",
    additional:
      "Предложение действует в течение 30 дней после покупки полиса ОСАГО с Mafin",
    href: "/kasko-sure",
    desktopImg: "d-kasko-offer-minikasko.jpg",
    tabletImg: "m-kasko-offer-minikasko.jpg",
  },
  {
    label: "Акция завершена",
    title: "Сверхподдержка по КАСКО",
    description:
      "Возместим любые дополнительные расходы, связанные со страховым случаем",
    additional:
      "Осуществляется поддержка по акции для полисов, оформленных с 28.10.2020 по 31.12.2020n",
    href: "/kasko-sure",
    desktopImg: "d-kasko-offer-extrasupport.jpg",
    tabletImg: "m-kasko-offer-extrasupport.jpg",
  },
];

const slides = offers.map(offer => (
  <SwiperSlide key={offer.title} className={styles.single__slide}>
    <Offer
      key={offer.title}
      label={offer.label}
      title={offer.title}
      description={offer.description}
      additional={offer.additional}
      href={offer.href}
      desktopImg={offer.desktopImg}
      tabletImg={offer.tabletImg}
    />
  </SwiperSlide>
));

export const Slider: FunctionComponent = () => (
  <div className={styles.swiper__container}>
    <div>
      <Swiper
        slidesPerView={2}
        spaceBetween={20}
        className={styles.swiper}
        updateOnWindowResize
        navigation={{
          disabledClass: styles.hidden,
          nextEl: `.${styles.arrow_next}`,
          prevEl: `.${styles.arrow_prev}`,
        }}
        pagination={{
          el: `.${styles.pagination}`,
          type: "bullets",
          bulletElement: "span",
          bulletClass: styles.pagination_bullet,
          bulletActiveClass: styles.pagination_bullet_active,
          clickable: true,
        }}
        breakpoints={{
          320: {
            width: 280,
            slidesPerView: 1,
          },
          375: {
            width: 315,
            slidesPerView: 1,
          },
          768: {
            width: 672,
            slidesPerView: 2,
          },
          1024: {
            width: 980,
            slidesPerView: 2,
          },
          1440: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
        }}
      >
        {slides}
      </Swiper>
    </div>
    <div className={styles.arrow_prev} />
    <div className={styles.arrow_next} />
    <div
      className={classNames("swiper-pagination-bullets", styles.pagination)}
    />
  </div>
);
