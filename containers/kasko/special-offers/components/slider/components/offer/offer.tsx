import React, { FunctionComponent } from "react";

import { breakpoints } from "@/utils/constants";

import styles from "./offer.module.scss";

export interface IOfferProps {
  label: string;
  title: string;
  description: string;
  additional: string;
  href: string;
  desktopImg: string;
  tabletImg: string;
}

export const Offer: FunctionComponent<IOfferProps> = ({
  label,
  title,
  description,
  additional,
  href,
  desktopImg,
  tabletImg,
}) => (
  <a href={href}>
    <div className={styles.offer}>
      <div className={styles.offer__image}>
        <picture>
          <source
            media={`(min-width: ${breakpoints.desktopSmall}px)`}
            srcSet={`/next-public-fast/images/kasko/special-offers/${desktopImg}`}
          />
          <source
            media={`(max-width: ${breakpoints.desktopSmall - 1}px)`}
            srcSet={`/next-public-fast/images/kasko/special-offers/${tabletImg}`}
          />
          <img className={styles.img} alt="" />
        </picture>
      </div>
      <div className={styles.offer__content}>
        <div className={styles.offer__content__top}>
          <div className={styles.offer__content__top_tablet}>
            <p className={styles.offer__label}>{label}</p>
            <p className={styles.offer__title}>{title}</p>
          </div>
          <p className={styles.offer__description}>{description}</p>
        </div>
        <div className={styles.offer__content__bottom}>
          <p className={styles.offer__additional}>{additional}</p>
          <p className={styles.offer__href}>Подробнее</p>
        </div>
      </div>
    </div>
  </a>
);
