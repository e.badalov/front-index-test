import React, { FunctionComponent } from "react";

import { Banner } from "@/containers/kasko/banner";
import { GRZform } from "@/components/grz-form/grz-form";

import styles from "./index.module.scss";

export const Promo: FunctionComponent = () => (
  <div className={styles.kasko__top}>
    <Banner />
    <div className={styles.kasko__top_grz}>
      <GRZform
        title="Введите номер автомобиля
                  для&nbsp;быстрого расчета цены"
        btntext="Рассчитать КАСКО"
        bgcolor="#ffffff"
      />
    </div>
  </div>
);
