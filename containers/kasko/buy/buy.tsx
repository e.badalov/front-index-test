import React, { FunctionComponent } from "react";

import { Steps } from "@/containers/kasko/steps";
import { GRZform } from "@/components/grz-form";

import styles from "./buy.module.scss";

export const Buy: FunctionComponent = () => (
  <div className={styles.kasko}>
    <section className={styles.kasko__section}>
      <h2
        data-autotest-id="kasko-kupit-polis-prosto"
        className={styles.kasko__section__title}
      >
        Купить полис КАСКО с&nbsp;Mafin просто
      </h2>
      <Steps />
      <GRZform
        className={styles.center}
        title="Получите персональную цену КАСКО,
              введите номер автомобиля"
        btntext="Рассчитать КАСКО"
        bgcolor="#EEF1F6"
        greyTitle
      />
    </section>
  </div>
);
