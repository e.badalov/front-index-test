import React from "react";

import Done from "@/assets/icons/done.svg";

import styles from "./checked-text.module.scss";

export interface CheckedTextProps {
  text: string;
  testId: string;
}

export const CheckedText: React.FunctionComponent<CheckedTextProps> = ({
  text,
  testId,
}) => (
  <div
    className={styles.checked_text}
    data-autotest-id={`stoa__checked-item__${testId}`}
  >
    <Done /> <div className={styles.text}>{text}</div>
  </div>
);
