import React from "react";

import { Button } from "@/components/button";
import { CheckedText } from "./components";

import styles from "./stoa.module.scss";

export const Stoa: React.FunctionComponent = () => (
  <div className={styles.wrapper} data-autotest-id="stoa">
    <div className={styles.text}>
      <h2 className={styles.title} data-autotest-id="h2-stoa__title">
        Широкий выбор официальных <br className={styles.mobile} /> сервисных
        центров
      </h2>
      <div className={styles.checked} data-autotest-id="stoa__checked">
        <CheckedText
          text="Согласование ремонта онлайн за 1 час"
          testId="reconciliation"
        />
        <CheckedText text="Ремонт до 100 000 руб. без справок" testId="fix" />
        <CheckedText
          text={`Проверка качества ремонтных работ\nперед выдачей авто`}
          testId="quality"
        />
      </div>
      <div className={styles.show_all}>
        <Button
          href="https://mafin.ru/kasko/stoa"
          data-autotest-id="stoa__show-all"
          bordered
        >
          Все сервисные центры
        </Button>
      </div>
    </div>
    <a href="/kasko/stoa">
      <div className={styles.map} data-autotest-id="stoa__map" />
    </a>
  </div>
);
