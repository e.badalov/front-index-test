import React, { FunctionComponent, useState, useRef } from "react";

import { Popup } from "./components/popup";
import styles from "./video.module.scss";

export const Video: FunctionComponent = () => {
  const vidRef1 = useRef<HTMLVideoElement>(null);
  const vidRef2 = useRef<HTMLVideoElement>(null);
  const vidRef3 = useRef<HTMLVideoElement>(null);

  const vidMobileRef1 = useRef<HTMLVideoElement>(null);
  const vidMobileRef2 = useRef<HTMLVideoElement>(null);
  const vidMobileRef3 = useRef<HTMLVideoElement>(null);

  const playBtnRef1 = useRef<HTMLDivElement>(null);
  const playBtnRef2 = useRef<HTMLDivElement>(null);
  const playBtnRef3 = useRef<HTMLDivElement>(null);

  const [shownPopup, setShownPopup] = useState<number>(-1);

  const hidePopup = (index: number) => {
    manageDesktopVideo(index, "pause");
    setShownPopup(-1);
  };

  const openPopup = (index: number) => {
    setShownPopup(index);
    manageDesktopVideo(index, "play");
  };

  function manageDesktopVideo(index: number, action: string) {
    let currentRef;

    switch (index) {
      case 1:
        currentRef = vidRef1;
        break;
      case 2:
        currentRef = vidRef2;
        break;
      case 3:
        currentRef = vidRef3;
        break;
      default:
        break;
    }

    if (currentRef && currentRef.current) {
      switch (action) {
        case "play":
          currentRef.current.play();
          break;
        case "pause":
          currentRef.current.pause();
          break;
        default:
          break;
      }
    }
  }

  const playVideo = (index: number) => {
    let currentVideoRef;
    let currentPlayBtnRef;

    switch (index) {
      case 1:
        currentVideoRef = vidMobileRef1;
        currentPlayBtnRef = playBtnRef1;
        break;
      case 2:
        currentVideoRef = vidMobileRef2;
        currentPlayBtnRef = playBtnRef2;
        break;
      case 3:
        currentVideoRef = vidMobileRef3;
        currentPlayBtnRef = playBtnRef3;
        break;
      default:
        break;
    }

    if (
      currentVideoRef &&
      currentVideoRef.current &&
      currentPlayBtnRef &&
      currentPlayBtnRef.current
    ) {
      currentVideoRef.current.play();
      currentVideoRef.current.controls = true;
      currentPlayBtnRef.current.style.display = "none";
    }
  };

  return (
    <section className={styles.video}>
      <div className={styles.video__container}>
        <h2 className={styles.video__title} data-autotest-id="h2-video__title">
          Видеоинструкции Mafin
        </h2>
        <div className={styles.video__content}>
          <div className={styles.video__content__desktop}>
            <div className={styles.video__item}>
              <div className={styles.video__item__container}>
                <div
                  className={styles.video__item__play}
                  onClick={() => openPopup(1)}
                  onKeyPress={() => openPopup(1)}
                  role="button"
                  tabIndex={0}
                  aria-label="Open video"
                />
                <Popup show={shownPopup === 1} onClose={() => hidePopup(1)}>
                  <video
                    ref={vidRef1}
                    className={styles.popup__video}
                    poster="/images/kasko/video/how-it-work.png"
                    controls
                  >
                    <source
                      src="https://mafin.ru/themes/mafin-site/assets/video/how-it-work.mp4"
                      type="video/mp4"
                    />
                  </video>
                </Popup>
                <img
                  className={styles.video__item__image}
                  src="/next-public-fast/images/kasko/video/how-it-work.png"
                  alt="Mafin how it works"
                />
              </div>
              <p className={styles.video__item__title}>
                Как оформить полис с&nbsp;Mafin
              </p>
            </div>

            <div className={styles.video__item}>
              <div className={styles.video__item__container}>
                <div
                  className={styles.video__item__play}
                  onClick={() => openPopup(2)}
                  onKeyPress={() => openPopup(2)}
                  role="button"
                  tabIndex={0}
                  aria-label="Open video"
                />
                <Popup show={shownPopup === 2} onClose={() => hidePopup(2)}>
                  <video
                    ref={vidRef2}
                    className={styles.popup__video}
                    poster="/images/kasko/video/how-to-inspect.png"
                    controls
                  >
                    <source
                      src="https://mafin.ru/themes/mafin-site/assets/video/how_to_inspect.mp4"
                      type="video/mp4"
                    />
                  </video>
                </Popup>
                <img
                  className={styles.video__item__image}
                  src="/next-public-fast/images/kasko/video/how-to-inspect.png"
                  alt="Mafin how to inspect a car"
                />
              </div>
              <p className={styles.video__item__title}>
                Как провести онлайн осмотр автомобиля в&nbsp;Mafin?
              </p>
            </div>

            <div className={styles.video__item}>
              <div className={styles.video__item__container}>
                <div
                  className={styles.video__item__play}
                  onClick={() => openPopup(3)}
                  onKeyPress={() => openPopup(3)}
                  role="button"
                  tabIndex={0}
                  aria-label="Open video"
                />
                <Popup show={shownPopup === 3} onClose={() => hidePopup(3)}>
                  <video
                    ref={vidRef3}
                    className={styles.popup__video}
                    poster="/images/kasko/video/how-to-settle.png"
                    controls
                  >
                    <source
                      src="https://mafin.ru/themes/mafin-site/assets/video/how-to-settle.mp4"
                      type="video/mp4"
                    />
                  </video>
                </Popup>
                <img
                  className={styles.video__item__image}
                  src="/next-public-fast/images/kasko/video/how-to-settle.png"
                  alt="Mafin how to settle"
                />
              </div>
              <p className={styles.video__item__title}>
                Как урегулировать страховой случай в&nbsp;Mafin?
              </p>
            </div>
          </div>
          <div className={styles.video__content__tablet}>
            <div className={styles.video__item}>
              <div className={styles.video__item__container}>
                <div
                  className={styles.video__item__play}
                  ref={playBtnRef1}
                  onClick={() => playVideo(1)}
                  onKeyPress={() => playVideo(1)}
                  role="button"
                  tabIndex={0}
                  aria-label="Play video"
                />

                <video
                  ref={vidMobileRef1}
                  className={styles.popup__video}
                  poster="/images/kasko/video/how-it-work.png"
                >
                  <source
                    src="https://mafin.ru/themes/mafin-site/assets/video/how-it-work.mp4"
                    type="video/mp4"
                  />
                </video>
              </div>
              <p className={styles.video__item__title}>
                Как оформить полис с&nbsp;Mafin
              </p>
            </div>
            <div className={styles.video__item}>
              <div className={styles.video__item__container}>
                <div
                  className={styles.video__item__play}
                  ref={playBtnRef2}
                  onClick={() => playVideo(2)}
                  onKeyPress={() => playVideo(2)}
                  role="button"
                  tabIndex={0}
                  aria-label="Play video"
                />
                <video
                  ref={vidMobileRef2}
                  className={styles.popup__video}
                  poster="/images/kasko/video/how-to-inspect.png"
                >
                  <source
                    src="https://mafin.ru/themes/mafin-site/assets/video/how_to_inspect.mp4"
                    type="video/mp4"
                  />
                </video>
              </div>
              <p className={styles.video__item__title}>
                Как провести онлайн осмотр автомобиля в&nbsp;Mafin?
              </p>
            </div>
            <div className={styles.video__item}>
              <div className={styles.video__item__container}>
                <div
                  className={styles.video__item__play}
                  ref={playBtnRef3}
                  onClick={() => playVideo(3)}
                  onKeyPress={() => playVideo(3)}
                  role="button"
                  tabIndex={0}
                  aria-label="Play video"
                />

                <video
                  ref={vidMobileRef3}
                  className={styles.popup__video}
                  poster="/images/kasko/video/how-to-settle.png"
                >
                  <source
                    src="https://mafin.ru/themes/mafin-site/assets/video/how-to-settle.mp4"
                    type="video/mp4"
                  />
                </video>
              </div>
              <p className={styles.video__item__title}>
                Как урегулировать страховой случай в&nbsp;Mafin?
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
