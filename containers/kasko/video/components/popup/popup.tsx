import React, { FunctionComponent } from "react";
import classnames from "classnames";

import Close from "@/assets/icons/close.svg";

import styles from "./popup.module.scss";

export interface IPopupProps {
  show: boolean;
  onClose: () => void;
}

export const Popup: FunctionComponent<IPopupProps> = ({
  children,
  show,
  onClose,
}) => {
  React.useEffect(() => {
    document.body.style.overflow = show ? "hidden" : "auto";
  }, [show]);

  return (
    <div className={styles.popup}>
      <div
        className={classnames(
          styles.popup__fade,
          { [styles.hidden]: !show },
          { [styles.visible]: show },
        )}
      >
        <div className={styles.popup__content}>
          <Close className={styles.popup__close} onClick={onClose} />
          {children}
        </div>
      </div>
    </div>
  );
};
