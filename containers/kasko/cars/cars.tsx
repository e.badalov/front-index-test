import React from "react";
import classNames from "classnames";

import { Button } from "@/components/button";
import { CarItem } from "./car-item";
import styles from "./cars.module.scss";

const cars = [
  "Mitsubishi",
  "Mazda",
  "Ford",
  "Toyota",
  "BMW",
  "Skoda",
  "Nissan",
  "Volkswagen",
  "Hyundai",
  "Kia",
  "Opel",
  "Peugeot",
  "Subaru",
  "Renault",
  "Suzuki",
  "Volvo",
];

export const Cars: React.FunctionComponent = () => {
  const [showAll, setShowAll] = React.useState(false);

  const toggleShowAll = () => {
    setShowAll(!showAll);
  };

  const renderCars = () => cars.map(car => <CarItem label={car} key={car} />);

  const renderToggleButton = () => {
    if (showAll) return null;

    return (
      <div className={styles.show_button}>
        <Button
          bordered
          onClick={toggleShowAll}
          data-autotest-id="cars__show-mode__button"
        >
          Показать еще
        </Button>
      </div>
    );
  };

  return (
    <div className={styles.wrapper} data-autotest-id="cars__wrapper">
      <h2 className={styles.title} data-autotest-id="h2-cars__title">
        Оформите КАСКО на любимый авто с Mafin!
      </h2>

      <div
        className={classNames(styles.cars, {
          [styles.hidden]: !showAll,
        })}
        data-autotest-id="cars__cars"
      >
        {renderCars()}
      </div>
      {renderToggleButton()}
    </div>
  );
};
