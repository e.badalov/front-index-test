import React from "react";

import styles from "./car-item.module.scss";

export interface CarItemProps {
  label: string;
}

export const CarItem: React.FunctionComponent<CarItemProps> = ({ label }) => (
  <a href={`/kasko/${label.toLowerCase()}`}>
    <div className={styles.container} data-autotest-id={`cars__item__${label}`}>
      <div
        data-autotest-id={`cars__item__${label}__image`}
        data-brand={label.toLowerCase()}
        className={styles.img}
        style={{
          backgroundImage: `url('/next-public-fast/cars/${label.toLowerCase()}.svg')`,
        }}
      />
      <div
        className={styles.label}
        data-autotest-id={`cars__item__${label}__label`}
      >
        {label}
      </div>
    </div>
  </a>
);
