import React from "react";
import * as Popups from "../popups";

interface CardProps {
  img: string;
  mobileImg: string;
  title: string;
  popup?: JSX.Element;
}

export const slides: CardProps[] = [
  {
    img: "/next-public-fast/advantages/personal-price.png",
    mobileImg: "/next-public-fast/advantages/mobile/personal-price.png",
    title: `Персональная цена до 30%\nниже`,
    popup: <Popups.PersonalPrice />,
  },
  {
    img: "/next-public-fast/advantages/online-registration.png",
    mobileImg: "/next-public-fast/advantages/mobile/online-registration.png",
    title: `Онлайн-\nоформление \n24/7`,
    popup: <Popups.OnlineRegistration />,
  },
  {
    img: "/next-public-fast/advantages/repair.png",
    mobileImg: "/next-public-fast/advantages/mobile/repair.png",
    title: `Согласование\nремонта онлайн\nза 1 час`,
    popup: <Popups.Repair />,
  },
  {
    img: "/next-public-fast/advantages/reconciliation.png",
    mobileImg: "/next-public-fast/advantages/mobile/reconciliation.png",
    title: `Ремонт\nбез справок\nдо 100 000 руб.`,
    popup: <Popups.Reconciliation />,
  },
  {
    img: "/next-public-fast/advantages/official-dealer.png",
    mobileImg: "/next-public-fast/advantages/mobile/official-dealer.png",
    title: `Ремонт\nу официального\nдилера`,
    popup: <Popups.OfficialDealer />,
  },
  {
    img: "/next-public-fast/advantages/evacuation.png",
    mobileImg: "/next-public-fast/advantages/mobile/evacuation.png",
    title: `Бесплатная\nэвакуация`,
    popup: <Popups.Evacuation />,
  },
  {
    img: "/next-public-fast/advantages/commissioner.png",
    mobileImg: "/next-public-fast/advantages/mobile/commissioner.png",
    title: `Бесплатный\nаварийный\nкомиссар`,
    popup: <Popups.Commissioner />,
  },
  {
    img: "/next-public-fast/advantages/contact-center.png",
    mobileImg: "/next-public-fast/advantages/mobile/contact-center.png",
    title: `Круглосуточная\nслужба заботы\nо клиентах `,
    popup: <Popups.ContactCenter />,
  },
];
