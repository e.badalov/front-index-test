import React from "react";

import styles from "./popups.module.scss";

export const Repair: React.FunctionComponent = () => (
  <div>
    <div className={styles.title}>Согласование ремонта онлайн за 1 час</div>
    <div className={styles.content}>
      Если что-то случится с вашей машиной, ехать в офис на осмотр не придется.
      Просто подайте заявление о страховом случае в приложении Mafin. В течение
      часа вы получите направление на ремонт к официальному дилеру.
    </div>
  </div>
);
