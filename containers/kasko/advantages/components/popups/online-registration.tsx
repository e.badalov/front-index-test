import React from "react";

import styles from "./popups.module.scss";
import { CheckedText } from "./components";

export const OnlineRegistration: React.FunctionComponent = () => (
  <div>
    <div className={styles.title}>Онлайн-оформление 24/7</div>
    <div className={styles.content}>
      <CheckedText text="Не нужно никуда ехать — все можно сделать в приложении Mafin." />
      <CheckedText text="Осмотр авто тоже онлайн. Подробно расскажем, как снять фото и видео, и посмотрим их в течение часа." />
      <CheckedText text="Оплата полиса — банковской картой." />
      <CheckedText text="Сразу после оплаты высылаем полис на e-mail и сохраняем его в приложении Mafin." />
    </div>
  </div>
);
