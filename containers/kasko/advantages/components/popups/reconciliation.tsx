import React from "react";

import styles from "./popups.module.scss";

export const Reconciliation: React.FunctionComponent = () => (
  <div>
    <div className={styles.title}>Ремонт без справок до 100 000 руб.</div>
    <div className={styles.content}>
      <div className={styles.row}>
        Оценку повреждений и приблизительной стоимости ремонта проведем за 10
        минут.
      </div>
      <div className={styles.row}>
        Вы можете отремонтировать авто по КАСКО без справок. Причем
        неограниченное число раз!
      </div>
      <div className={styles.row}>
        Справки не нужны, если: {"\n"}— стоимость ремонта не превышает 100 000
        руб.; {"\n"}— нужно отремонтировать только стеклянные элементы и/или
        световые приборы; {"\n"}— нужно отремонтировать два смежных элемента
        кузова.
      </div>
    </div>
  </div>
);
