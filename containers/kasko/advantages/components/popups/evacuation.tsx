import React from "react";

import styles from "./popups.module.scss";

export const Evacuation: React.FunctionComponent = () => (
  <div>
    <div className={styles.title}>Бесплатная эвакуация</div>
    <div className={styles.content}>
      Вызовем эвакуатор, если ваш авто не может передвигаться самостоятельно
      после происшествия. Если по каким-то причинам у нас не получилось
      предоставить вам эвакуатор, возместим расходы на него пределах 5 000 руб.
      Если использовался эвакуатор-манипулятор — в пределах 12 000 руб. Опция
      доступна в любой точке мира.
    </div>
  </div>
);
