import React from "react";

import styles from "./popups.module.scss";

export const ContactCenter: React.FunctionComponent = () => (
  <div>
    <div className={styles.title}>Круглосуточная Служба заботы о клиентах</div>
    <div className={styles.content}>
      Расскажем, как действовать при страховом случае и какие документы нужно
      оформлять.
    </div>
  </div>
);
