import React from "react";

import styles from "./popups.module.scss";

export const Commissioner: React.FunctionComponent = () => (
  <div>
    <div className={styles.title}>Бесплатный аварийный комиссарс</div>
    <div className={styles.content}>
      Аварийный комиссар готов приехать на место происшествия, вызвать
      необходимые службы, оформить документы и заявить страховой случай в
      приложении Mafin.
    </div>
  </div>
);
