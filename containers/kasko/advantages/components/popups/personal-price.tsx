import React from "react";

import { Button } from "@/components/button";

import styles from "./popups.module.scss";
import { CheckedText } from "./components";

export const PersonalPrice: React.FunctionComponent = () => (
  <div>
    <div className={styles.title}>Персональная цена ниже до 30%</div>
    <div className={styles.content}>
      <CheckedText text="Формируем индивидуальную цену на основе ваших персональных данных и гарантируем их сохранность." />
      <CheckedText text="Берем в расчет больше факторов, чтобы вы платили меньше! Умный алгоритм анализирует множество факторов — от наличия штрафов до частоты поездок." />
      <CheckedText text="Не меняем рассчитанную вами цену в течение 60 дней. Так вы сможете сравнить наше предложение с рынком и спокойно принять решение." />
    </div>
    <Button
      bordered
      className={styles.popup__show}
      href="https://cdn.mafin.ru/pdf/best-price.pdf"
    >
      Подробнее
    </Button>
  </div>
);
