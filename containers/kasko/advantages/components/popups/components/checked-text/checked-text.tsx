import React from "react";

import Done from "@/assets/icons/done.svg";

import styles from "./checked-text.module.scss";

export interface CheckedTextProps {
  text: string;
}

export const CheckedText: React.FunctionComponent<CheckedTextProps> = ({
  text,
}) => (
  <div className={styles.wrapper}>
    <Done className={styles.icon} />
    <div className={styles.text}>{text}</div>
  </div>
);
