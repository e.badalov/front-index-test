import React from "react";

import styles from "./popups.module.scss";

export const OfficialDealer: React.FunctionComponent = () => (
  <div>
    <div className={styles.title}>Ремонт у официального дилера</div>
    <div className={styles.content}>
      Вы можете выбрать удобный для вас официальный сервис. Мы направляем наших
      клиентов только в проверенные автосервисы и тщательно контролируем
      качество работ. Когда ремонт будет завершен, наш специалист осмотрит
      автомобиль в автосервисе, чтобы убедиться в качестве ремонта.
    </div>
  </div>
);
