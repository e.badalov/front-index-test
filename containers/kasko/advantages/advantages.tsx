import React from "react";
import classNames from "classnames";

import { Swiper as SwiperView, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation, Pagination } from "swiper";

import { Button } from "@/components/button";
import { Popup } from "@/components/popup";
import { breakpoints } from "@/utils/contants";

import styles from "./advantages.module.scss";
import { slides } from "./components/slides";

SwiperCore.use([Pagination, Navigation]);

export const Advantages: React.FunctionComponent = () => {
  const [shownPopup, setShownPopup] = React.useState<number>();
  const showPopup = (index: number) => () => setShownPopup(index);
  const hidePopup = () => setShownPopup(-1);

  const renderCards = () =>
    slides.map(({ img, mobileImg, title }, index) => (
      <SwiperSlide
        aria-hidden
        onClick={showPopup(index)}
        key={title}
        className={styles.card}
      >
        <picture className={styles.picture}>
          <source
            media={`(min-width: ${breakpoints.desktopSmall}px)`}
            srcSet={img}
          />
          <source
            media={`(max-width: ${breakpoints.desktopSmall - 1}px)`}
            srcSet={mobileImg}
          />
          <img className={styles.img} alt="" width="1px" height="1px" />
        </picture>
        <div className={styles.title}>{title}</div>
        <a className={styles.show_more}>Подробнее</a>
      </SwiperSlide>
    ));

  const renderPopups = () =>
    slides.map(({ popup, title }, index) => (
      <Popup
        show={shownPopup === index}
        key={`popup_${title}`}
        onClose={hidePopup}
        autotestId={`popup_${index}`}
      >
        {popup}
      </Popup>
    ));

  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <h2
          data-autotest-id="h2-kasko-advantages__title"
          className={styles.title}
        >
          Преимущества КАСКО с Mafin
        </h2>
        <div className={styles.swiper__wrapper}>
          <SwiperView
            className={styles.swiper}
            width={1134}
            slidesPerView={4}
            spaceBetween={30}
            updateOnWindowResize
            navigation={{
              disabledClass: styles.hidden,
              nextEl: `.${styles.arrow_next}`,
              prevEl: `.${styles.arrow_prev}`,
            }}
            pagination={{
              el: `.${styles.pagination}`,
              type: "bullets",
              bulletElement: "span",
              bulletClass: styles.pagination_bullet,
              bulletActiveClass: styles.pagination_bullet_active,
              clickable: true,
            }}
            breakpoints={{
              320: {
                width: 153,
                spaceBetween: 10,
                slidesPerView: 1,
              },
              375: {
                width: 316,
                spaceBetween: 10,
                slidesPerView: 2,
              },
              768: {
                width: 672,
                spaceBetween: 20,
                slidesPerView: 4,
              },
              1024: {
                width: 840,
                spaceBetween: 20,
                slidesPerView: 3,
              },
              1440: {
                width: 1134,
                spaceBetween: 30,
                slidesPerView: 4,
              },
            }}
          >
            {renderCards()}
          </SwiperView>
          {renderPopups()}
          <div className={styles.arrow_prev} />
          <div className={styles.arrow_next} />
        </div>
        <div
          className={classNames("swiper-pagination-bullets", styles.pagination)}
        />
        <Button className={styles.calculate}>Рассчитать КАСКО</Button>
      </div>
    </div>
  );
};
