import React, { FunctionComponent } from "react";

import { Tooltip } from "@/components/tooltip";
import { Button } from "@/components/button";

import styles from "./app.module.scss";
import { Slider } from "./components/slider";

export const App: FunctionComponent = () => (
  <section className={styles.app}>
    <h2 className={styles.app__title} data-autotest-id="h2-application__title">
      Приложение Mafin
    </h2>
    <div className={styles.app__content}>
      <div className={styles.app__phone}>
        <div className={styles.app__phone__top} />
        <Slider />
      </div>
      <ul className={styles.app__list}>
        <li className={styles.app__list__item}>
          <div className={styles.app__list__item__tooltip}>
            Оформите полис без поездок в&nbsp;офис страховой компании
            <div className={styles.tooltip}>
              <Tooltip grey right>
                <p className={styles.tooltip__text}>
                  Чат-бот подскажет — какие документы нужны и поможет сделать
                  осмотр машины самостоятельно
                </p>
              </Tooltip>
            </div>
            <span className={styles.app__list__item__tablettext}>
              . Чат-бот подскажет — какие документы нужны и поможет сделать
              осмотр машины самостоятельно
            </span>
          </div>
        </li>
        <li className={styles.app__list__item}>
          Управляйте полисом онлайн в&nbsp;личном кабинете
        </li>
        <li className={styles.app__list__item}>
          Подайте все документы и&nbsp;запишитесь на&nbsp;ремонт
          при&nbsp;страховом случае
        </li>
      </ul>
      <div className={styles.app__link}>
        <div className={styles.app__qr__image} />
        <p className={styles.app__qr__text}>
          Скачать приложение Mafin легко - наведите камеру смартфона
          на&nbsp;QR-код
        </p>
        <Button
          className={styles.app__download__btn}
          data-autotest-id="download-app-btn"
          href="https://redirect.appmetrica.yandex.com/serve/819656514523459136"
        >
          Скачать приложение
        </Button>
      </div>
    </div>
  </section>
);
