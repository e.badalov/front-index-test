import React from "react";
import SwiperCore, { Autoplay } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

import styles from "./slider.module.scss";
import { Slide } from "./components/slide";

SwiperCore.use([Autoplay]);

const images = [
  {
    bgImage: "url('/next-public-fast/images/kasko/app/screen-1.png')",
  },
  {
    bgImage: "url('/next-public-fast/images/kasko/app/screen-2.png')",
  },
  {
    bgImage: "url('/next-public-fast/images/kasko/app/screen-3.png')",
  },
];

const slides = images.map(image => (
  <SwiperSlide key={image.bgImage}>
    <Slide bgImage={image.bgImage} />
  </SwiperSlide>
));

export function Slider() {
  return (
    <div className={styles.swiper__container}>
      <Swiper
        loop
        autoplay={{
          delay: 3000,
          disableOnInteraction: false,
        }}
        slidesPerView={1}
        spaceBetween={0}
        width={276}
        updateOnWindowResize
      >
        {slides}
      </Swiper>
    </div>
  );
}
