import React, { FunctionComponent } from "react";

import styles from "./slide.module.scss";

export interface ISlideProps {
  bgImage: string;
}

export const Slide: FunctionComponent<ISlideProps> = ({ bgImage }) => (
  <div className={styles.slide} style={{ backgroundImage: bgImage }} />
);
