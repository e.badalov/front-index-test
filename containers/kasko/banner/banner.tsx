import React, { FunctionComponent } from "react";
import classnames from "classnames";

import styles from "./banner.module.scss";

export const Banner: FunctionComponent = () => (
  <div className={classnames(styles.banner, styles.banner__kasko)}>
    <div className={styles.banner__container}>
      <h1 data-autotest-id="h1-banner-title" className={styles.banner__title}>
        Рассчитайте цену
        <br />
        КАСКО за&nbsp;2&nbsp;минуты
      </h1>
    </div>
  </div>
);
